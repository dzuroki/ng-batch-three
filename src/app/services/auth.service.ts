import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject, catchError, delay, map, Observable, tap, throwError } from 'rxjs';
import { environment } from 'src/environments/environment';

interface requestLogin {
  email: string, password: string
}

interface responseBase<T> {
  message: string,
  data: T
}

interface responseLogin {
  token: string,
  role: string,
}

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private readonly baseApi: string = environment.api

  private token: string = ''
  public isAuthenticated: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false)

  constructor(private httpClient: HttpClient) { 
    this.loadToken();
  }

  /**
   * @param payload payload username && password
   * @returns message / token
   */
  login(payload: requestLogin): Observable<responseBase<responseLogin>> {
    return this.httpClient.post<responseBase<responseLogin>>(`${this.baseApi}/users/login`, payload).pipe(
      tap( () => {
        this.isAuthenticated.next(true)
      })
    )
  }

  /**
   * 
   * @param payload 
   * @returns 
   */
  register(payload: requestLogin) {
    return this.httpClient.post(`${this.baseApi}`, payload)
  }

  loadToken() {
    const token = localStorage.getItem('token');
    if(token) {
      this.token = token
      this.isAuthenticated.next(true)
    } else {
      this.isAuthenticated.next(false)
    }
  }
}
