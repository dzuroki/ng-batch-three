import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class EmployeesService {
  private readonly baseApi = 'https://reqres.in';

  constructor(private http: HttpClient) {}

  getListEmployee() {
    const headers = new HttpHeaders({
      'api-key': 'API-52MQ6ZR0WETG2ZQ',
      'accept': 'application/json'
    });
    return this.http
      .get(`http://alltheclouds.com.au/api/Products`, { headers })
      .pipe(map((val) => val));
  }

  addEmployee(payload: { name: string; job: string }) {
    return this.http.post(`${this.baseApi}/api/users`, payload);
  }
}
