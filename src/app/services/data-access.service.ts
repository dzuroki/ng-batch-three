import { Injectable } from '@angular/core';
import { Person } from '../layout/admin/playground/playground.utils';

@Injectable({
  providedIn: 'root'
})
export class DataAccessService {

  private person: Person[] = [
    {
      name: 'Dzurrahman',
      age: 20,
    },
    {
      name: 'Roki',
      age: 21,
    },
  ];

  constructor() { }

  getPerson(): Person[] {
    return this.person
  }

  addPerson(payload: Person) {
    this.person.push(payload);
  }

  deletePerson(payload: Person) {
    return this.person = this.person.filter( val => val !== payload)
  }


} 
