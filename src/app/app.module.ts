import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { HotToastModule } from '@ngneat/hot-toast';
import { ComponentsModule } from './components/components.module';
import { LottieModule } from 'ngx-lottie';
import player from 'lottie-web';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { ModalModule } from 'ngx-bootstrap/modal';
import { NZ_I18N } from 'ng-zorro-antd/i18n';
import { id_ID } from 'ng-zorro-antd/i18n';
import { registerLocaleData } from '@angular/common';
import id from '@angular/common/locales/id';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

registerLocaleData(id);

export const playerFactory = () => {
  return player;
};

export const HttpLoaderFactory = (http: HttpClient) => {
  return new TranslateHttpLoader(http)
}
@NgModule({
  declarations: [AppComponent],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    HotToastModule.forRoot(),
    ComponentsModule,
    LottieModule.forRoot({ player: playerFactory }),
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      }
    }),
    ModalModule.forRoot(),
    BrowserAnimationsModule
  ],
  providers: [
    { provide: NZ_I18N, useValue: id_ID }
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
