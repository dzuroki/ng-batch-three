import { Component } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'ng-batch-three';

  constructor(private translateService: TranslateService) {
    const defaultLang: string = localStorage.getItem('lang') ?? 'en'
    this.translateService.setDefaultLang(defaultLang)
    this.translateService.use(defaultLang)
  }
}
