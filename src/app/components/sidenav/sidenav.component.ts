import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-sidenav',
  templateUrl: './sidenav.component.html',
  styleUrls: ['./sidenav.component.scss'],
  // changeDetection: ChangeDetectionStrategy.OnPush
})
export class SidenavComponent implements OnInit {

  isCollapsed = false;

  toggleCollapsed(): void {
    this.isCollapsed = !this.isCollapsed;
  }
  
  constructor() { }

  ngOnInit(): void {
    console.log('SidenavComponent');
  }

  doLog() {
    console.log('SidenavComponent');
  }

}
