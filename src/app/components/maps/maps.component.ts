import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import Map from 'ol/Map';
import View from 'ol/View';
import TileLayer from 'ol/layer/Tile';
import OSM from 'ol/source/OSM';
import { transform } from 'ol/proj.js';

import { Geolocation } from '@capacitor/geolocation';

const printCurrentPosition = async () => {
  const coordinates = await Geolocation.getCurrentPosition();

  console.log('Current position:', coordinates);
  return coordinates
};
@Component({
  selector: 'app-maps',
  templateUrl: './maps.component.html',
  styleUrls: ['./maps.component.scss']
})
export class MapsComponent implements OnInit, OnDestroy {

  @Input() loc = {
    longitude: 107.6071,
    latitude: -6.9217
  }
  map!: Map;

  constructor() { }

  async ngOnInit() {
    // const result = await printCurrentPosition()
    this.map = new Map({
      view: new View({
        center: transform([this.loc.longitude, this.loc.latitude], 'EPSG:4326', 'EPSG:3857'),
        zoom: 18,
      }),
      layers: [
        new TileLayer({
          source: new OSM(),
        }),
      ],
      target: 'ol-map'
    });
    this.map.updateSize()
  }

  ngOnDestroy() {
    this.map.dispose();
  }

}
