import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { CardNameComponent } from './card-name/card-name.component';
import { RouterModule } from '@angular/router';
import { SidenavComponent } from './sidenav/sidenav.component';
import { TranslateModule } from '@ngx-translate/core';
import { MapsComponent } from './maps/maps.component';
import { NzMenuModule } from 'ng-zorro-antd/menu';
import { NzToolTipModule } from 'ng-zorro-antd/tooltip';
import { NzIconModule } from 'ng-zorro-antd/icon';
import { NzButtonModule } from 'ng-zorro-antd/button';
import {
  MenuFoldOutline,
  MenuUnfoldOutline
} from '@ant-design/icons-angular/icons';
import { IconDefinition } from '@ant-design/icons-angular';
const icons: IconDefinition[] = [MenuFoldOutline, MenuUnfoldOutline];

@NgModule({
  declarations: [
    HeaderComponent,
    FooterComponent,
    CardNameComponent,
    SidenavComponent,
    MapsComponent
  ],
  imports: [
    CommonModule,
    RouterModule,
    TranslateModule,
    NzMenuModule,
    NzToolTipModule,
    NzButtonModule,
    NzIconModule.forChild(icons)
  ],
  exports: [
    HeaderComponent,
    FooterComponent,
    CardNameComponent,
    SidenavComponent,
    MapsComponent
  ]
})
export class ComponentsModule { }
