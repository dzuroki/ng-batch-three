import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
  // changeDetection: ChangeDetectionStrategy.OnPush
})
export class HeaderComponent implements OnInit {

  constructor(private translateService: TranslateService) { }

  ngOnInit(): void {
    console.log('HeaderComponent');
  }

  setLang(ev: string) {
    localStorage.setItem('lang', ev)
    this.translateService.setDefaultLang(ev)
    this.translateService.use(ev)
  }

  doLog() {
    console.log('HeaderComponent');
  }


}
