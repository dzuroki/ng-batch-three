import { ChangeDetectionStrategy, Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Person } from 'src/app/layout/admin/playground/playground.utils';

@Component({
  selector: 'app-card-name',
  templateUrl: './card-name.component.html',
  styleUrls: ['./card-name.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CardNameComponent implements OnInit {

  @Input() data: Person = {
    name: '',
    age: 0
  }

  @Output() cardValueDelete = new EventEmitter()
  @Output() cardValueEdit = new EventEmitter()

  constructor() { }

  ngOnInit(): void {
    console.log('CardNameComponent');
  }

  doDelete() {
    console.log('doDelete ', this.data.name)
    this.cardValueDelete.emit(this.data)
  }

  doEdit() {
    this.cardValueEdit.emit(this.data)
  }

  doLog() {
    console.log('CardNameComponent' , this.data.name);
  }

}
