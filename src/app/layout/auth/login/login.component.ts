import { Component, OnDestroy, OnInit } from '@angular/core';
import { AuthService } from 'src/app/services/auth.service';
import { HotToastService } from '@ngneat/hot-toast';
import { Router } from '@angular/router';
import { firstValueFrom, Subject, takeLast, takeUntil } from 'rxjs';
import { AnimationOptions } from 'ngx-lottie';
import {
  AbstractControl,
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from '@angular/forms';

interface FormLogin {
  username: string | null;
  password: string | null;
}
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit, OnDestroy {
  username: string = '';
  password: string = '';
  isPass: boolean = true;

  formLogin: FormGroup;

  cancelLogin$ = new Subject<void>();

  optionsConfig: AnimationOptions = {
    path: 'assets/lottie/login-lottie.json',
  };

  constructor(
    private authService: AuthService,
    private toastService: HotToastService,
    private router: Router
  ) {
    this.formLogin = new FormGroup({
      username: new FormControl<string>('dzurrahman@gmail.com', {
        validators: [Validators.required, Validators.email],
      }),
      password: new FormControl<string | null>('123456', {
        validators: [Validators.required, Validators.minLength(6)],
      }),
    });
  }

  ngOnInit(): void {
    // console.log(this.ngZone)
    null
  }

  ngOnDestroy(): void {
    this.cancelLogin$.next();
    this.cancelLogin$.complete();
    this.cancelLogin$.unsubscribe();
  }

  get errorControl() {
    return this.formLogin.controls;
  }

  doChangeTypePass() {
    this.isPass = !this.isPass;
  }

  doLogin() {
    console.log(this.formLogin.value);
    const payload = {
      email: this.formLogin.value.username,
      password: this.formLogin.value.password,
    };
    this.authService
      .login(payload)
      .pipe(takeUntil(this.cancelLogin$), this.toastService.observe({
        loading: 'Please Wait',
        success: (s) => s.message,
        error: (err) => 'Oops! Something Went Wrong Error' + err.error.message ?? err.message ?? ''
      }))
      .subscribe({
        next: (val) => {
          console.log(val);
          localStorage.setItem('token', JSON.stringify(val.data));
          if (val.data.role === 'admin') {
            this.router.navigate(['admin/home']);
          } else {
            this.toastService.error(
              `Oops! You're <strong>${val.data.role.toUpperCase()}</strong> have no access this site`,
              { dismissible: true, duration: 6000 }
            );
          }
        },
        error: (err) => {
          console.log(err);
          let message = err.error.message ?? err.message;
          if (err.status === 0) {
            message = 'Oops! Something Went Wrong Error';
            console.log(window);
          }
          localStorage.setItem('token', 'no-token');
        },
      });
  }

  async doLoginAsync() {
    try {
      this.toastService.loading('Login...');
      const payload = {
        email: this.username,
        password: this.password,
      };
      const result = await firstValueFrom(this.authService.login(payload));
      if (result) {
        localStorage.setItem('token', result.data.token);
        this.toastService.close();
        this.toastService.success(result.message);
      }
    } catch (err: any) {
      console.log(err);
      this.toastService.close();
      this.toastService.error(err.error.message);
    }
  }
}
