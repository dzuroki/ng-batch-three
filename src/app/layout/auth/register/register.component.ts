import { Component, OnInit } from '@angular/core';
import { AnimationOptions } from 'ngx-lottie';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {

  optionsConfig: AnimationOptions = {
    path: 'assets/lottie/register-lottie.json',
  };

  constructor() { }

  ngOnInit(): void {
    console.log('RegisterComponent');
  }

}
