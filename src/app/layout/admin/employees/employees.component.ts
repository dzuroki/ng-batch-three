import { Component, OnInit, TemplateRef } from '@angular/core';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { firstValueFrom } from 'rxjs';
import { EmployeesService } from 'src/app/services/employees.service';

@Component({
  selector: 'app-employees',
  templateUrl: './employees.component.html',
  styleUrls: ['./employees.component.scss']
})
export class EmployeesComponent implements OnInit {

  empList: any = []
  modalRef?: BsModalRef;

  name = '';
  job = '';
  isEdit = false
  constructor(private modalService: BsModalService, private employeesService: EmployeesService) {}
  
  ngOnInit(): void {
    this.doGetEmployeeList();
  }
 
  openModal(template: TemplateRef<any>, data?: any) {
    console.log(data);
    if(data) {
      this.isEdit = true
      this.name = data.first_name + data.last_name
      this.job = data.email
    } else {
      this.isEdit = false
    }

    this.modalRef = this.modalService.show(template);
  }

  async doGetEmployeeList() {
    try {
      const result: any = await firstValueFrom(this.employeesService.getListEmployee())
      this.empList = result
    } catch (error) {
      
    }
  }

  doAddEmployee() {
    const payload = {
      name: this.name,
      job: this.job
    }
    this.employeesService.addEmployee(payload).subscribe({
      next: (val) => {
        alert(val)
        this.doGetEmployeeList();
      }
    })
  }

}
