import {
  AfterViewInit,
  Component,
  DoCheck,
  OnDestroy,
  OnInit,
  TemplateRef,
  ViewChild,
} from '@angular/core';
import {
  interval,
  Observable,
  Subject,
  Subscriber,
  Subscription,
  takeUntil,
} from 'rxjs';
import { DataAccessService } from 'src/app/services/data-access.service';
import { Person } from './playground.utils';
import { BsModalService, BsModalRef, ModalOptions } from 'ngx-bootstrap/modal';
import Map from 'ol/Map';
import View from 'ol/View';
import TileLayer from 'ol/layer/Tile';
import OSM from 'ol/source/OSM';
import { transform } from 'ol/proj.js';

import { Geolocation } from '@capacitor/geolocation';
import { MapsComponent } from 'src/app/components/maps/maps.component';

const printCurrentPosition = async () => {
  const coordinates = await Geolocation.getCurrentPosition();

  console.log('Current position:', coordinates);
  return coordinates;
};

interface BengkelData {
  nama: string,
  latitude: number,
  longitude: number,
}

@Component({
  selector: 'app-playground',
  templateUrl: './playground.component.html',
  styleUrls: ['./playground.component.scss'],
})
export class PlaygroundComponent implements OnInit, OnDestroy, AfterViewInit {
  name: string = '';
  age: number = 0;
  show: boolean = true;
  isEdit: boolean = false;

  person: Person[] = [];

  timer = interval(100);
  isDestroy = new Subject();
  test!: Subscription;

  modalRef?: BsModalRef;
  map!: Map;

  dataBengkel: BengkelData[] = [
    {
      nama: 'Astra bengkel 1',
      latitude: -6.9174639,
      longitude: 107.6191228,
    },
    {
      nama: 'Astra bengkel 2',
      latitude: -8.9174639,
      longitude: 107.6191228,
    }
  ];

  @ViewChild('olMap') olMap!: { nativeElement: any };

  constructor(
    private dataAccessService: DataAccessService,
    private modalService: BsModalService
  ) {
    // this.test = this.timer.subscribe( val => {
    //   console.log(val);

    // })
    console.log('constructor');
  }

  async ngOnInit() {
    // this.person = this.dataAccessService.getPerson()
    // this.timer.pipe(takeUntil(this.isDestroy)).subscribe( val => {
    //   console.log(val);
    // }

    // firstValueFrom(this.timer).then( val => console.log(val))
    console.log('ngOnInit');
  }

  // ngDoCheck() {
  //   console.log('ngDoCheck');
  // }

  async ngAfterViewInit() {
    console.log('ngAfterViewInit');
  }

  ngOnDestroy() {
    this.isDestroy.next(true);
    this.isDestroy.complete();
    // this.test.unsubscribe();
    console.log('ngOnDestroy');
  }

  openModalWithComponent(data: BengkelData) {
    const initialState: ModalOptions = {
      initialState: {
        loc: {
          longitude: data.longitude,
          latitude: data.latitude,
        },
      },
    };
    this.modalRef = this.modalService.show(MapsComponent, initialState);
    this.modalRef.content.closeBtnName = 'Close';
  }

  doAddPerson() {
    if (this.name === '') return alert('Name must be no null');
    if (this.age < 0) return alert('Age must be no minus');
    const payload = {
      name: this.name,
      age: this.age,
    };
    this.dataAccessService.addPerson(payload);
    this.formReset();
  }

  formReset() {
    (this.name = ''), (this.age = 0);
  }

  doDelete(ev: Person) {
    this.person = this.dataAccessService.deletePerson(ev);
  }

  doEdit(ev: Person) {
    this.isEdit = true;
    this.name = ev.name;
    this.age = ev.age;
  }

  doUpdatePerson() {
    this.isEdit = false;
    this.formReset();
  }

  doShow() {
    this.show ? (this.show = false) : (this.show = true);
  }
}
