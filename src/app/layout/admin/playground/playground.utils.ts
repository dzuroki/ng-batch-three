export interface Person {
    name: string,
    age: number,
    status?: boolean
}