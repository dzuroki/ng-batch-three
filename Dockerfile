# Stage 1
# FROM node:lts-alpine as build-step
# WORKDIR /usr/app
# COPY package.json package-lock.json .npmrc ./
# RUN npm ci
# COPY . .
# RUN npm run build

# FROM nginx:1.17.1-alpine
# COPY nginx.config /etc/nginx/conf.d/default.conf
# COPY ./dist/ng-batch-three /usr/share/nginx/html